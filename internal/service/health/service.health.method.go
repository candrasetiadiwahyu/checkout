package health

import (
	"gitlab.com/candrasetiadiwahyu/checkout/graph/generated"
)

func (s *Service) CheckHealth() (*generated.OrderHealth, error) {
	return &generated.OrderHealth{
			Message: "SUCCESS",
		},
		nil
}
