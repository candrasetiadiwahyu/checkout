package checkout

//go:generate mockgen -source=service.checkout.dependencies.go -package=checkout -destination=service.checkout.dependencies_mock_test.go

import (
	"context"

	itemsModel "gitlab.com/candrasetiadiwahyu/checkout/internal/model/items"
)

type itemsRepository interface {
	FindItemBySKU(ctx context.Context, sku string) (data itemsModel.ItemsModel, err error)
}
