package checkout

// CheckoutServiceImpl struct implementation
type CheckoutServiceImpl struct {
	itemsRepository itemsRepository
}

// NewCheckoutService constructor
func NewCheckoutService(
	itemsRepository itemsRepository,
) *CheckoutServiceImpl {
	return &CheckoutServiceImpl{
		itemsRepository: itemsRepository,
	}
}
