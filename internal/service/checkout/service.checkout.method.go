package checkout

import (
	"context"

	"gitlab.com/candrasetiadiwahyu/checkout/graph/generated"
	model "gitlab.com/candrasetiadiwahyu/checkout/internal/model/items"
)

// Checkout func to checkout
func (o *CheckoutServiceImpl) Checkout(ctx context.Context, req []*generated.CheckoutRequest) (result *generated.CheckoutResponse, err error) {
	var listSKU []string
	for _, v := range req {
		listSKU = append(listSKU, v.Sku)
	}
	totalAmount, err := o.calculatePromo(ctx, listSKU, req)
	if err != nil {
		return
	}
	return &generated.CheckoutResponse{
		TotalAmount: totalAmount,
	}, nil
}

func (o *CheckoutServiceImpl) calculatePromo(ctx context.Context, sku []string, req []*generated.CheckoutRequest) (totalAmount float64, err error) {
	if contains(sku, "43N23P") && contains(sku, "234234") {
		return o.promo1(ctx, req)
	} else if contains(sku, "120P90") {
		return o.promo2(ctx, req)
	} else if contains(sku, "A304SD") {
		return o.promo3(ctx, req)
	} else {
		return o.withoutPromo(ctx, req)
	}
}

func (o *CheckoutServiceImpl) withoutPromo(ctx context.Context, req []*generated.CheckoutRequest) (amount float64, err error) {
	for _, v := range req {
		var data model.ItemsModel
		data, err = o.itemsRepository.FindItemBySKU(ctx, v.Sku)
		if err != nil {
			return
		}
		amount = amount + (data.Price * float64(v.Quantity))
	}
	return
}

func (o *CheckoutServiceImpl) promo3(ctx context.Context, req []*generated.CheckoutRequest) (amount float64, err error) {
	for _, v := range req {
		var data model.ItemsModel
		data, err = o.itemsRepository.FindItemBySKU(ctx, v.Sku)
		if err != nil {
			return
		}
		price := data.Price * float64(v.Quantity)
		if v.Sku == "A304SD" && v.Quantity >= 3 {
			price = price - (price * 0.1)
		}
		amount = amount + price
	}
	return
}

func (o *CheckoutServiceImpl) promo2(ctx context.Context, req []*generated.CheckoutRequest) (amount float64, err error) {
	for _, v := range req {
		var data model.ItemsModel
		data, err = o.itemsRepository.FindItemBySKU(ctx, v.Sku)
		if err != nil {
			return
		}
		qty := v.Quantity
		if v.Sku == "120P90" && qty == 3 {
			qty = 2
		}
		amount = amount + (data.Price * float64(qty))
	}
	return
}

func (o *CheckoutServiceImpl) promo1(ctx context.Context, req []*generated.CheckoutRequest) (amount float64, err error) {
	for _, v := range req {
		if v.Sku == "234234" {
			continue
		} else {
			var data model.ItemsModel
			data, err = o.itemsRepository.FindItemBySKU(ctx, v.Sku)
			if err != nil {
				return
			}
			amount = amount + (data.Price * float64(v.Quantity))
		}
	}
	return
}

// contains checks if a string is present in a slice
func contains(s []string, str string) bool {
	for _, v := range s {
		if v == str {
			return true
		}
	}

	return false
}
