package items

import (
	"context"
	"encoding/json"

	model "gitlab.com/candrasetiadiwahyu/checkout/internal/model/items"
)

func mockDataItems() []byte {
	items := "[{\"sku\":\"120P90\",\"name\":\"Google Home\",\"price\":49.99,\"qty\":10},{\"sku\":\"43N23P\",\"name\":\"MacBook Pro\",\"price\":5399.99,\"qty\":5},{\"sku\":\"A304SD\",\"name\":\"Alexa Speaker\",\"price\":109.5,\"qty\":10},{\"sku\":\"234234\",\"name\":\"Raspberry Pi B\",\"price\":30,\"qty\":2}]"
	bytes := []byte(items)
	return bytes
}

// FindItemBySKU func
func (i *ItemsRepositoryImpl) FindItemBySKU(ctx context.Context, sku string) (data model.ItemsModel, err error) {
	items := mockDataItems()
	var dataItems []model.ItemsModel
	json.Unmarshal(items, &dataItems)

	for _, v := range dataItems {
		if v.SKU == sku {
			data = model.ItemsModel{
				SKU:   v.SKU,
				Name:  v.Name,
				Price: v.Price,
				Qty:   v.Qty,
			}
			break
		}
	}
	return data, err
}
