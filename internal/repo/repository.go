package repo

import (
	"context"

	"gitlab.com/cms-tech/be-toolkit/lib/sql/sqlwrap"
)

const (
	// ContextKeyDBConns context key dor db conns
	ContextKeyDBConns ContextKey = "db_conns"
)

// ContextKey type for context key
type ContextKey string

type DbContext struct {
	DbConn sqlwrap.DBConns
}

// MysqlReadConn get read connection
func (d DbContext) MysqlSlaveConn(ctx context.Context) sqlwrap.Queryer {
	dbConn := ctx.Value(ContextKeyDBConns)
	if dbConn == nil {
		return d.DbConn.Slave()
	}

	return dbConn.(sqlwrap.Queryer)
}

// MysqlWriteConn get read connection
func (d DbContext) MysqlMasterConn(ctx context.Context) sqlwrap.Queryer {
	dbConn := ctx.Value(ContextKeyDBConns)
	if dbConn == nil {
		return d.DbConn.Master()
	}

	return dbConn.(sqlwrap.Queryer)
}

// SetDBConnInContext set db connection in context
func SetDBConnInContext(ctx context.Context, db sqlwrap.DBTransactor) context.Context {
	return context.WithValue(ctx, ContextKeyDBConns, db)
}
