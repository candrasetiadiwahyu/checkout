package middleware

import (
	"context"
	"log"
	"net/http"

	"gitlab.com/candrasetiadiwahyu/checkout/internal/model/header"
)

var xHeader = &header.ContextKey{Name: "header"}

// Middleware decodes the share session cookie and packs the session into context
func Middleware() func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			xUserID := r.Header.Get("user-id")
			if xUserID == "" {
				next.ServeHTTP(w, r)
				return
			}
			userXUserID := header.Header{
				UserID: xUserID,
			}

			// put it in context
			ctx := context.WithValue(r.Context(), xHeader, &userXUserID)
			// and call the next with our new context
			r = r.WithContext(ctx)
			next.ServeHTTP(w, r)
		})
	}
}

// ForContext finds the user from the context. REQUIRES Middleware to have run.
func ForContext(ctx context.Context) *header.Header {
	raw, err := ctx.Value(xHeader).(*header.Header)
	if !err {
		log.Println("ERROR get context value: ", err)
	}

	return raw
}
