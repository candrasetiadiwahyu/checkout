package resolver

import (
	"context"

	"gitlab.com/candrasetiadiwahyu/checkout/graph/generated"
)

//go:generate mockgen -source=resolver.go -package=resolver -destination=resolver.mock_test.go

// This file will not be regenerated automatically.
//
// It serves as dependency injection for your app, add any dependencies you require here.

type healthServiceInterface interface {
	CheckHealth() (*generated.OrderHealth, error)
}

// CheckoutServiceInterface interface
type CheckoutServiceInterface interface {
	Checkout(ctx context.Context, req []*generated.CheckoutRequest) (result *generated.CheckoutResponse, err error)
}

// Resolver struct
type Resolver struct {
	healthService   healthServiceInterface
	checkoutService CheckoutServiceInterface
}

// NewResolver constructor
func NewResolver(
	healthService healthServiceInterface,
	checkoutService CheckoutServiceInterface,
) *Resolver {
	return &Resolver{
		healthService:   healthService,
		checkoutService: checkoutService,
	}
}
