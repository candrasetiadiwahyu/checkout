package resolver

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"

	"gitlab.com/candrasetiadiwahyu/checkout/graph/generated"
)

func (r *mutationResolver) Checkout(ctx context.Context, req []*generated.CheckoutRequest) (*generated.CheckoutResponse, error) {
	return r.checkoutService.Checkout(ctx, req)
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

type mutationResolver struct{ *Resolver }
