package utility

import "database/sql"

// StringToNullString string to null string
func StringToNullString(str string) sql.NullString {
	if str == "" {
		return sql.NullString{}
	}

	return sql.NullString{
		String: str,
		Valid:  true,
	}
}
