package utility

import (
	"time"

	"fmt"
)

//TimeLapse struct
type TimeLapse struct {
	StartTime time.Time
}

//StartTimer func
func StartTimer() TimeLapse {
	return TimeLapse{
		StartTime: time.Now(),
	}
}

//GetTimeLapseSecond function to get timelapse in second
func (t TimeLapse) GetTimeLapseSecond() float64 {
	return time.Since(t.StartTime).Seconds()
}

// GetTimeNowIDN function to get time now UTC + 7
func GetTimeNowIDN() time.Time {
	nowUTC := time.Now().UTC()
	nowIDN := nowUTC.Add(time.Hour * 7)
	return nowIDN
}

// ConvertToTargetTimezone convert to target timezone
func ConvertToTargetTimezone(datetime string, currTimezone int32, targetTimezone int32) (time.Time, error) {
	timezone := "UTC"
	if currTimezone < 0 {
		timezone += "-%d"
	} else {
		timezone += "+%d"
	}
	tz := fmt.Sprintf(timezone, currTimezone)
	loc := time.FixedZone(tz, int(currTimezone)*60*60)
	arrTime, err := time.ParseInLocation(DefaultDateTimeFormat, datetime, loc)
	if err != nil {
		return time.Now(), err
	}

	timezone = "UTC"
	if targetTimezone < 0 {
		timezone += "-%d"
	} else {
		timezone += "+%d"
	}
	tz = fmt.Sprintf(timezone, targetTimezone)
	return arrTime.In(time.FixedZone(tz, int(targetTimezone)*60*60)), nil
}

// ConvertTimeToMillisecond time to millisecond
func ConvertTimeToMillisecond(times time.Time) int64 {
	return times.UnixNano() / int64(time.Millisecond)
}

// GetCurrentTimeGMT7 func to get GMT+7 timezone
func GetCurrentTimeGMT7() (time.Time, error) {
	t := time.Now()
	// get the location
	location, err := time.LoadLocation("Asia/Jakarta")
	if err == nil {
		t = t.In(location)
	}
	// this should give you time in location
	return t, err
}
