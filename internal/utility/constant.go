package utility

const (
	// DefaultDateTimeFormat const
	DefaultDateTimeFormat string = "2006-01-02 15:04:05"
	// DefaultDateFormat const
	DefaultDateFormat string = "2006-01-02"
)

var ORDER_STATUS = map[int]string{
	1: "NEW",
	2: "WAITING_FOR_PAYMENT",
	3: "PAID",
	4: "EXPIRED",
	5: "CANCELED",
	6: "REFUNDED",
	7: "REJECTED",
}

var ORDER_TYPE = map[int]string{
	1: "EVENT",
	2: "MEMBERSHIP",
}
