package health

type Health struct {
	UserID  int64  `json:"id"`
	Message string `json:"message"`
}
