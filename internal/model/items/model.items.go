package items

type (
	// ItemsModel struct
	ItemsModel struct {
		SKU   string  `json:"sku"`
		Name  string  `json:"name"`
		Price float64 `json:"price"`
		Qty   int64   `json:"qty"`
	}
)
