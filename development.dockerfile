FROM golang:1.16
ENV FILE_LOC=./.dev/.gql.air.toml
ENV SERVICE=checkout

RUN apt update && apt upgrade -y && \
    apt install -y git \
    make openssh-client

WORKDIR /go/src/gitlab.com/${SERVICE}

RUN curl -fLo install.sh -v https://raw.githubusercontent.com/cosmtrek/air/master/install.sh \
    && chmod +x install.sh && sh install.sh && cp ./bin/air /bin/air
CMD air -c $FILE_LOC