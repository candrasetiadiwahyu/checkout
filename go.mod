module gitlab.com/candrasetiadiwahyu/checkout

go 1.16

require (
	github.com/99designs/gqlgen v0.13.0
	github.com/DATA-DOG/go-sqlmock v1.5.0 // indirect
	github.com/Masterminds/squirrel v1.5.0 // indirect
	github.com/go-chi/chi v3.3.2+incompatible
	github.com/go-sql-driver/mysql v1.6.0
	github.com/golang/mock v1.5.0
	github.com/stretchr/testify v1.7.0
	github.com/vektah/gqlparser/v2 v2.2.0
	gitlab.com/cms-tech/be-toolkit v0.0.0-20210705112342-7173a8908476
	gopkg.in/yaml.v2 v2.4.0
)
