package main

import (
	"database/sql"
	"fmt"
	"time"

	"gitlab.com/candrasetiadiwahyu/checkout/internal/config"
	"gitlab.com/cms-tech/be-toolkit/lib/sql/sqlwrap"
)

// NewDbConn return new instace of module for MySql
func NewDbConn(cfg *config.Config) sqlwrap.DBConns {
	conn := fmt.Sprintf("%s%s", GetMysqlConn(cfg.Connection.MySQLMaster), "?parseTime=true")
	dbMaster, err := sql.Open("mysql", conn)
	if err != nil {
		fmt.Println(err.Error())
	}

	dbMaster.SetMaxOpenConns(cfg.Connection.MySQLMaster.MaxOpenConn)
	dbMaster.SetMaxIdleConns(cfg.Connection.MySQLMaster.MaxIdleConn)
	dbMaster.SetConnMaxLifetime(time.Second * 20)

	conn = fmt.Sprintf("%s%s", GetMysqlConn(cfg.Connection.MySQLSlave), "?parseTime=true")
	dbSlave, err := sql.Open("mysql", conn)
	if err != nil {
		fmt.Println(err.Error())
	}

	dbSlave.SetMaxOpenConns(cfg.Connection.MySQLSlave.MaxOpenConn)
	dbSlave.SetMaxIdleConns(cfg.Connection.MySQLSlave.MaxIdleConn)
	dbSlave.SetConnMaxLifetime(time.Second * 20)

	dbConns := sqlwrap.NewDBConns(sqlwrap.New(dbSlave, false), sqlwrap.New(dbMaster, false))
	dbConns.SetNoSlave(cfg.Connection.IsNoSlave)
	return dbConns
}

// GetMysqlConn is func to get mysql connection
func GetMysqlConn(cfg config.MysqlConfig) string {
	return fmt.Sprintf(
		"%s:%s@%s(%s:%s)/%s",
		cfg.Username,
		cfg.Password,
		cfg.Protocol,
		cfg.Host,
		cfg.Port,
		cfg.Name)
}
