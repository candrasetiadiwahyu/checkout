package main

import (
	"log"
	_ "net/http/pprof"

	_ "github.com/go-sql-driver/mysql"
	"gitlab.com/candrasetiadiwahyu/checkout/internal/config"
)

const repoName = "checkout"

func main() {

	log.Println("Initialize configuration")
	cfg, err := config.New(repoName)
	if err != nil {
		log.Fatalf("failed to init the config: %v", err)
	}

	startApp(cfg)
}
