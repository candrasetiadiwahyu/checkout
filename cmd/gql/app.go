package main

import (
	"github.com/99designs/gqlgen/graphql/handler"
	"gitlab.com/candrasetiadiwahyu/checkout/graph/generated"
	"gitlab.com/candrasetiadiwahyu/checkout/internal/config"
	"gitlab.com/candrasetiadiwahyu/checkout/internal/resolver"

	itemsRepo "gitlab.com/candrasetiadiwahyu/checkout/internal/repo/items"
	checkoutSrv "gitlab.com/candrasetiadiwahyu/checkout/internal/service/checkout"
	healthSrv "gitlab.com/candrasetiadiwahyu/checkout/internal/service/health"

	"gitlab.com/cms-tech/be-toolkit/lib/log"
)

func startApp(cfg *config.Config) {
	// log.Infoln("Initialize connection to database")
	// dbCOnn := NewDbConn(cfg)

	log.Infoln("Initialize repository")
	itemsRepository := itemsRepo.NewItemsRepository()

	log.Infoln("Initialize service")
	healthService := healthSrv.New()
	checkoutService := checkoutSrv.NewCheckoutService(itemsRepository)

	log.Infoln("Initialize Resolver")
	resolver := resolver.NewResolver(
		healthService,
		checkoutService,
	)

	handlerSrv := handler.NewDefaultServer(generated.NewExecutableSchema(generated.Config{Resolvers: resolver}))

	router := newRoutes(handlerSrv, resolver)
	startServer(router, cfg)
}
