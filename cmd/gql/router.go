package main

import (
	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
	"github.com/go-chi/chi"
	"gitlab.com/candrasetiadiwahyu/checkout/internal/middleware"
	"gitlab.com/candrasetiadiwahyu/checkout/internal/resolver"
)

func newRoutes(handlerSrv *handler.Server, resolver *resolver.Resolver) *chi.Mux {
	router := chi.NewRouter()
	router.Use(middleware.Middleware())
	router.Handle("/", playground.Handler("GraphQL playground", "/query"))
	router.Handle("/query", handlerSrv)

	return router
}
